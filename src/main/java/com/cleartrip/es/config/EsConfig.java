package com.cleartrip.es.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AbstractFactoryBean;
import org.springframework.context.annotation.Configuration;

@Configuration
//@EnableElasticsearchRepositories(basePackages = "com.cleartrip.model.repository")
public class EsConfig extends AbstractFactoryBean<RestHighLevelClient> {

	@Autowired
	EsConstants esConstants;

	private static final Logger LOG = LoggerFactory.getLogger(EsConfig.class);

	RestHighLevelClient restHighLevelClient;

	@Override
	public void destroy() {
		try {
			if (restHighLevelClient != null) {
				restHighLevelClient.close();
			}
		} catch (final Exception e) {
			LOG.error("Error closing ElasticSearch client: ", e);
		}
	}

	@Override
	public Class<RestHighLevelClient> getObjectType() {
		return RestHighLevelClient.class;
	}

	@Override
	public boolean isSingleton() {
		return false;
	}

	@Override
	public RestHighLevelClient createInstance() {
		return buildClient();
	}

	private RestHighLevelClient buildClient() {
		try {
			restHighLevelClient = new RestHighLevelClient(RestClient.builder(new HttpHost(esConstants.getEsHost(),
					Integer.valueOf(esConstants.getEsPort()), esConstants.getEsSchema())));
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error(e.getMessage());
		}
		return restHighLevelClient;
	}

}