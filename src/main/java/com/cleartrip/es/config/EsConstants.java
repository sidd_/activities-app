package com.cleartrip.es.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class EsConstants {

	public EsConstants() {
	}

	@Value("${elasticsearch.clustername}")
	private String clusterName;

	@Value("${elasticsearch.host}")
	private String esHost;

	@Value("${elasticsearch.port}")
	private String esPort;

	@Value("${elasticsearch.schema}")
	private String esSchema;

	public String getClusterName() {
		return clusterName;
	}

	public String getEsHost() {
		return esHost;
	}

	public String getEsPort() {
		return esPort;
	}

	public String getEsSchema() {
		return esSchema;
	}

//	public static final String UUID = "UUID";
//	public static final String SEARCH_INSTANCE_ID = "sid";

}
