package com.cleartrip.activitiesapp;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthCheckController {
	
	@RequestMapping(value = "/health-check")
	public String healthCheck() {
		return "Success - activities-app is up";
	}
}
